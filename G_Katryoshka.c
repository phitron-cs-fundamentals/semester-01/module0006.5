#include <stdio.h>

int main() {
    int e, m, b, sum = 0;
    scanf("%d %d %d", &e, &m, &b);
    int mn = (e < m) ? ((e < b) ? e : b) : ((m < b) ? m : b);
    sum += mn;
    e -= mn;
    m -= mn;
    b -= mn;
    mn = (e / 2 < b) ? (e / 2) : b;
    sum += mn;
    printf("%d\n", sum);
    return 0;
}